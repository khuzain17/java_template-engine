package com.example.restapi.latihan12.service.implementation;

import com.example.restapi.latihan12.entity.Employee;
import com.example.restapi.latihan12.service.EmployeeRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class EmployeeRestTemplateImpl implements EmployeeRestTemplateService {
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Override
    public Map insert(Employee employee) {
        Map<Object, Object> map = new HashMap<Object, Object>();
        try {
            String url = "http://localhost:8080/synrgy/employee/save";

            ResponseEntity<Map> result = restTemplateBuilder.build().postForEntity(url, employee, Map.class);

            map.put("data", result);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee Added successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(Employee employee) {
        Map<Object, Object> map = new HashMap<Object, Object>();
        try {
            String url = "http://localhost:8080/synrgy/employee/updatebyid/" + employee.getId();

            HttpEntity<Employee> req = new HttpEntity<>(employee);
            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.PUT,req, new
                    ParameterizedTypeReference<Map>(){});

            map.put("data", result);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee Added successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long idemployee) {
        Map<Object, Object> map = new HashMap<Object, Object>();
        try {
            String url = "http://localhost:8080/synrgy/employee/delete/" + idemployee;

            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url,
                    HttpMethod.DELETE, null, new
                    ParameterizedTypeReference<Map>(){});

            map.put("data", result);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee Added successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getData() {
        Map<Object, Object> map = new HashMap<Object, Object>();
        try {
            String url = "http://localhost:8080/synrgy/employee/all";

            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.GET,null, new
                    ParameterizedTypeReference<Map>(){});

            map.put("data", result);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee get all successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getById(Long idemployee) {
        return null;
    }
}
