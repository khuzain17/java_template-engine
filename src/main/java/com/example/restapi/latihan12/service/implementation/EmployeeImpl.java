package com.example.restapi.latihan12.service.implementation;

import com.example.restapi.latihan12.entity.Employee;
import com.example.restapi.latihan12.entity.EmployeeDetail;
import com.example.restapi.latihan12.entity.Rekening;
import com.example.restapi.latihan12.repository.EmployeeDetailRepo;
import com.example.restapi.latihan12.repository.EmployeeRepo;
import com.example.restapi.latihan12.repository.RekeningRepo;
import com.example.restapi.latihan12.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


import java.util.*;

//Step-step:
//1. Tambahkan anotasi @Service dan @Transactional
//2. Jangan lupa menambahkan anotasi @Autowired sebagia DI
//3. definisikan Map map sebagai wadah menampung message, status, dan data
//4. lakukan save jika sudah memasukkan data
//5. map.put untuk memasukkan data
//6. dapat melakukan pengecekan null untuk mengetahui data kosong atau tidak

@Service
@Transactional
public class EmployeeImpl implements EmployeeService {

    @Autowired
    public EmployeeRepo repo;

    @Autowired
    public EmployeeDetailRepo employeeDetailRepo;

    @Autowired
    public RekeningRepo rekeningRepo;

    @Override
    public Map insert(Employee employee) {
        Map map = new HashMap();
        try {
            EmployeeDetail employeeDetail = employeeDetailRepo.save(employee.getEmployeeDetail());
            Employee obj = repo.save(employee);

            employeeDetail.setEmployee(obj);
            employeeDetailRepo.save(employeeDetail);

            map.put("data", obj);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee created successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(Employee employee) {
        Map map = new HashMap();
        try {
            Employee emp = repo.getById(employee.getId());


//            EmployeeDetail employeeDetail = employeeDetailRepo.save(employee.getEmployeeDetail());
//            Employee obj = repo.save(employee);

//            employeeDetail.setEmployee(obj);
//            employeeDetailRepo.save(employeeDetail);


            if (emp == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Employee does not exist");
                return map;
            }

            emp.setName(employee.getName());
            emp.setSex(employee.getSex());
            emp.setBirthDate(employee.getBirthDate());
            emp.setAddress(employee.getAddress());
            emp.setStatus(employee.getStatus());
            repo.save(emp);

            map.put("data", emp);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee updated successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long employeeId) {
        Map map = new HashMap();
        try {
            Employee emp = repo.getById(employeeId);
            if (emp == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Employee does not exist");
                return map;
            }

            if (emp.getEmployeeDetail() != null) {
                EmployeeDetail employeeDetail= employeeDetailRepo.getById(emp.getEmployeeDetail().getId());
                employeeDetail.setDeleted_date(new Date());
                employeeDetailRepo.save(employeeDetail);
            }

//            karna relasinya one to many jadi gak bisa get Rekening ID
            if (emp.getRekening() != null) {
//                map.put("empRekening", emp.getRekening());
//                map.put("empRekening", emp.getRekening()); // anda tidak bisa map put tiap nilai dgn key yg sama...

                for (int i = 0; i <  emp.getRekening().size(); i++) {
                    Rekening rek= rekeningRepo.getById(emp.getRekening().get(i).getId());

                    rek.setDeleted_date(new Date());
                    rekeningRepo.save(rek);
                }
            }

            emp.setDeleted_date(new Date());
            repo.save(emp);

            map.put("data", emp);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee deleted successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getAll() {
        List<Employee> employees = new ArrayList<Employee>();
        Map map = new HashMap();
        try {
            employees = repo.getAllEmployee();
            map.put("data", employees);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getByStatus(int status) {
        List<Employee> employees = new ArrayList<Employee>();
        Map map = new HashMap();
        try {
            employees = repo.getByStatus(status);
            map.put("data", employees);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getById(Long employeeId) {
        Map map = new HashMap();
        try {
            Employee emp = repo.getById(employeeId);
            if (emp == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Employee does not exist");
                return map;
            }

            map.put("data", emp);
            map.put("statusCode", "200");
            map.put("statusMessage", "Employee get successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public List<Employee> listEmployee(int pageNumber, int ROW_PER_PAGE) {
        List<Employee> karyawans = new ArrayList<>();
        Pageable sortedIdASC = PageRequest.of(pageNumber - 1, ROW_PER_PAGE, Sort.by("id").ascending());
        repo.findAll(sortedIdASC).forEach(karyawans::add);
        return karyawans;
    }

    @Override
    public Long count() {
        return repo.count();
    }

    @Override
    public Employee findById(Long karyawanId) {
        try {
            Employee obj = repo.getById(karyawanId);

            return obj;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
