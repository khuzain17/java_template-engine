package com.example.restapi.latihan12.service.implementation;

import com.example.restapi.latihan12.entity.Employee;
import com.example.restapi.latihan12.entity.EmployeeDetail;
import com.example.restapi.latihan12.repository.EmployeeDetailRepo;
import com.example.restapi.latihan12.repository.EmployeeRepo;
import com.example.restapi.latihan12.service.EmployeeDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class EmployeeDetailImpl implements EmployeeDetailService {
    @Autowired
    EmployeeDetailRepo repo;

    @Autowired
    EmployeeRepo employeeRepo;

    @Override
    public Map insert(EmployeeDetail employeeDetail, Long idEmployee) {
        Employee objemp = employeeRepo.getById(idEmployee);
        employeeDetail.setEmployee(objemp);

        Map map = new HashMap();
        try {
            EmployeeDetail employeeDet = repo.save(employeeDetail);
            map.put("data", employeeDet);
            map.put("statusCode", "200");
            map.put("statusMessage", "Detail created successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(EmployeeDetail employeeDetail) {
        Map map = new HashMap();
        try {
            EmployeeDetail empd = repo.getById(employeeDetail.getId());

            if (empd == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Rekening does not exist");
                return map;
            }
            empd.setNik(employeeDetail.getNik());
            empd.setNpwp(employeeDetail.getNpwp());

            repo.save(empd);

            map.put("data", empd);
            map.put("statusCode", "200");
            map.put("statusMessage", "Detail Employee updated successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long idEmployeeDetail) {
        Map map = new HashMap();
        try {
            EmployeeDetail employeeDet = repo.getById(idEmployeeDetail);

            if (employeeDet == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Detail does not exist");
                return map;
            }
            employeeDet.setDeleted_date(new Date());
            repo.save(employeeDet);

            map.put("statusCode", "200");
            map.put("statusMessage", "Detail deleted successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getById(Long idEmployeeDetail) {
        Map map = new HashMap();
        try {
            EmployeeDetail employeeDetail = repo.getById(idEmployeeDetail);

            if (employeeDetail == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Detail does not exist");
                return map;
            }

            map.put("data", employeeDetail);
            map.put("statusCode", "200");
            map.put("statusMessage", "Data Employee Detail");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getAll() {
        List<EmployeeDetail> employeeDetails = new ArrayList<EmployeeDetail>();
        Map map = new HashMap();
        try {
            employeeDetails = repo.getAllEmployeeDetail();
            map.put("data", employeeDetails);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success get all info");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
}
