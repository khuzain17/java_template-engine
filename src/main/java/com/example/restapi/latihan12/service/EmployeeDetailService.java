package com.example.restapi.latihan12.service;

import com.example.restapi.latihan12.entity.EmployeeDetail;

import java.util.Map;

public interface EmployeeDetailService {
    public Map insert(EmployeeDetail employeeDetail, Long idEmployee);

    public Map update(EmployeeDetail employeeDetail);

    public Map delete(Long idEmployeeDetail);

    public Map getById(Long idEmployeeDetail);

    public Map getAll();
}
