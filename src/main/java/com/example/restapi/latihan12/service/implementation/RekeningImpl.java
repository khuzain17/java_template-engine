package com.example.restapi.latihan12.service.implementation;

import com.example.restapi.latihan12.entity.Employee;
import com.example.restapi.latihan12.entity.Rekening;
import com.example.restapi.latihan12.repository.EmployeeRepo;
import com.example.restapi.latihan12.repository.RekeningRepo;
import com.example.restapi.latihan12.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class RekeningImpl implements RekeningService {
    @Autowired
    public RekeningRepo repo;

    @Autowired
    EmployeeRepo employeeRepo;


    @Override
    public Map insert(Rekening rekening, Long idKaryawan) {
        Employee objemp = employeeRepo.getById(idKaryawan);
        rekening.setEmployee(objemp);

        Map map = new HashMap();
        try {
            Rekening rek = repo.save(rekening);
            map.put("data", rek);
            map.put("statusCode", "200");
            map.put("statusMessage", "Rekening created successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map update(Rekening rekening) {
        Map map = new HashMap();
        try {
            Rekening rek = repo.getById(rekening.getId());

            if (rek == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Rekening does not exist");
                return map;
            }

            rek.setNamaBank(rekening.getNamaBank());
            rek.setNomor(rekening.getNomor());
            rek.setJenis(rekening.getJenis());
            repo.save(rek);

            map.put("data", rek);
            map.put("statusCode", "200");
            map.put("statusMessage", "Rekening updated successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long idRekening) {
        Map map = new HashMap();
        try {
            Rekening rekening = repo.getById(idRekening);

            if (rekening == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Rekening does not exist");
                return map;
            }
            rekening.setDeleted_date(new Date());
            repo.save(rekening);

            map.put("statusCode", "200");
            map.put("statusMessage", "Rekening deleted successfully");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getById(Long idRekening) {
        Map map = new HashMap();
        try {
            Rekening rekening = repo.getById(idRekening);

            if (rekening == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Rekening does not exist");
                return map;
            }

            map.put("data", rekening);
            map.put("statusCode", "200");
            map.put("statusMessage", "Data Rekening");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getAll() {
        List<Rekening> rekenings = new ArrayList<Rekening>();
        Map map = new HashMap();
        try {
            // masih jadi pertimbangan
//            Rekening rek = repo.updateDeletedDate();
            rekenings = repo.getAllRekening();

            map.put("data", rekenings);
            map.put("statusCode", "200");
            map.put("statusMessage", "Success get all info");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }

    }
}
