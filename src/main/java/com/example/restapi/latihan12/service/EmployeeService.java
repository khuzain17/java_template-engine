package com.example.restapi.latihan12.service;

import com.example.restapi.latihan12.entity.Employee;

import java.util.List;
import java.util.Map;

//Step-step:
//1. Bagian ini berisi mathod kosongan yang nanti akan dipanggil di implementasi
//2. ini yang akan digenerate secara otomatis ketika di extends

public interface EmployeeService {

    public Map insert(Employee employee);

    public Map update(Employee employee);

    public Map delete(Long employeeId);

    public Map getAll();

    public Map getByStatus(int status);

    public Map getById(Long employeeId);

    public List<Employee> listEmployee(int pageNumber, int ROW_PER_PAGE);

    public Long count();

    public Employee findById(Long karyawanId);

}
