package com.example.restapi.latihan12.controller;

import com.example.restapi.latihan12.entity.Employee;
import com.example.restapi.latihan12.repository.EmployeeRepo;
import com.example.restapi.latihan12.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("synrgy/employee")
public class EmployeeController {

    @Autowired
    EmployeeService service;

    @Autowired
    public EmployeeRepo employeeRepo;

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<Map> getAll() {
        Map employees = service.getAll();
        return new ResponseEntity<Map>(employees, HttpStatus.OK);
    }

    //sorting by @requestParam
    @GetMapping("/all-sorting")
    @ResponseBody
    public ResponseEntity<Map> getAllSorting(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String nameShort,
            @RequestParam() String typeShort
    ) {
        Map map = new HashMap();
        String getshorting = nameShort.equals("")?"id": nameShort;
        Pageable show_data;
        if(typeShort.equals("desc")) {
            show_data = PageRequest.of(page, size, Sort.by(getshorting).descending());
        } else  {
            show_data = PageRequest.of(page, size, Sort.by(getshorting).ascending());
        }
        map.put("data", employeeRepo.findAll(show_data));
        return new ResponseEntity<Map>(map, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/all/{status}")
    @ResponseBody
    public ResponseEntity<Map> getByStatus(@PathVariable(value = "status") int status) {
        Map employees = service.getByStatus(status);
        return new ResponseEntity<Map>(employees, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Employee newEmployee) {
        Map map = new HashMap();
        Map emp = service.insert(newEmployee);

        map.put("Request = ", newEmployee);
        map.put("Response = ", emp);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody Employee employeeToUpdate) {
        Map map = new HashMap();
        Map emp = service.update(employeeToUpdate);

        map.put("Request = ", employeeToUpdate);
        map.put("Response = ", emp);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

    @PutMapping("/updatebyid/{id}")
    public ResponseEntity<Map> updateById(@PathVariable("id") Long id, @Valid @RequestBody Employee employeeToUpdate) {
        employeeToUpdate.setId(id);

        Map map = new HashMap();
        Map emp = service.update(employeeToUpdate);

        map.put("Request = ", employeeToUpdate);
        map.put("Response = ", emp);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map emp = service.delete(id);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

    @GetMapping("/all/id/{id}")
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map emp = service.getById(id);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

}
