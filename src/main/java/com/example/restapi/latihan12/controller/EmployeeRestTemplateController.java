package com.example.restapi.latihan12.controller;

import com.example.restapi.latihan12.entity.Employee;
import com.example.restapi.latihan12.service.EmployeeRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("synrgy/rest-template")
public class EmployeeRestTemplateController {
    @Autowired
    EmployeeRestTemplateService service;

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<Map> getAll() {
        Map employeesrt = service.getData();
        return new ResponseEntity<Map>(employeesrt, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Employee newEmployee) {
        Map map = new HashMap();
        Map emp = service.insert(newEmployee);

        map.put("Request = ", newEmployee);
        map.put("Response = ", emp);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id, @Valid @RequestBody Employee mployeeupdate) {
        mployeeupdate.setId(id);
        Map obj = service.update(mployeeupdate);

        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id) {
        Map obj = service.delete(id);

        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody Employee mployeeupdate) {
        Map obj = service.update(mployeeupdate);

        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

}
