package com.example.restapi.latihan12.controller.ThymeLeaf;

import com.example.restapi.latihan12.entity.Employee;
import com.example.restapi.latihan12.entity.EmployeeDetail;
import com.example.restapi.latihan12.repository.EmployeeDetailRepo;
import com.example.restapi.latihan12.repository.EmployeeRepo;
import com.example.restapi.latihan12.service.EmployeeRestTemplateService;
import com.example.restapi.latihan12.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller // Bukan RestController
@RequestMapping("/v1/view")
public class ListEmployeeController {
    @Autowired
    EmployeeService employeeService;

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    EmployeeDetailRepo employeeDetailRepo;

    private final int ROW_PER_PAGE = 5;

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        model.addAttribute("title", "Employee");
        return "index";
    }

    @GetMapping(value = "/employee")
    public String getEmployee(Model model, @RequestParam(value = "page", defaultValue = "1") int pageNumber) {
        List<Employee> employeeList = employeeService.listEmployee(pageNumber, ROW_PER_PAGE);

        long count = employeeService.count();
        boolean hasPrev = pageNumber > 1;
        boolean hasNext = (pageNumber * ROW_PER_PAGE) < count;
        model.addAttribute("employeelst", employeeList);
        model.addAttribute("hasPrev", hasPrev);
        model.addAttribute("prev", pageNumber - 1);
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("next", pageNumber + 1);
        return "employee-list";
    }

    @GetMapping(value = {"/employee/add"})
    public String showAddemployee(Model model) {
        Employee employee = new Employee();
        model.addAttribute("add", true);
        model.addAttribute("employee", employee);
        return "employee-edit";
    }

    @GetMapping(value = "/employee/{employeeId}")
    public String getemployeeById(Model model, @PathVariable Long employeeId) {
        Employee employee = null;
        try {
            employee = employeeService.findById(employeeId);
        } catch (Exception ex) {
            model.addAttribute("errorMessage", "employee not found");
        }
        model.addAttribute("employee", employee);
        return "employee";
    }

    @PostMapping(value = "/employee/add")
    public String addemployee(Model model,
                              @ModelAttribute("employee") Employee employee) {
        try {
            EmployeeDetail detailemployee = employee.getEmployeeDetail();

            if (detailemployee != null) {
                detailemployee = employeeDetailRepo.save(detailemployee);
            }
            Employee newemployee = employeeRepo.save(employee);
            detailemployee.setEmployee(newemployee);
            employeeDetailRepo.save(detailemployee);
            return "redirect:/v1/view/employee/" + String.valueOf(newemployee.getId());
        } catch (Exception ex) {
            String errorMessage = ex.getMessage();
            model.addAttribute("errorMessage", errorMessage);
            model.addAttribute("add", true);
            return "employee-edit";
        }
    }

    @GetMapping(value = {"/employee/{employeeId}/edit"})
    public String showEditemployee(Model model, @PathVariable Long employeeId) {
        Employee employee = null;
        try {
            employee = employeeService.findById(employeeId);

        } catch (Exception ex) {
            model.addAttribute("errorMessage", "Emp not found");
        }
        model.addAttribute("add", false);
        model.addAttribute("employee", employee);
        return "employee-edit";
    }

    @PostMapping(value = {"/employee/{employeeId}/edit"})
    public String updateemployee(Model model,
                                 @PathVariable Long employeeId,
                                 @ModelAttribute("employee") Employee employee) {
        try {
            employee.setId(employeeId);
            employeeService.update(employee);
            return "redirect:/v1/view/employee/" + String.valueOf(employee.getId());
        } catch (Exception ex) {

            String errorMessage = ex.getMessage();
            model.addAttribute("errorMessage", errorMessage);

            model.addAttribute("add", false);
            return "employee-edit";
        }
    }

    @GetMapping(value = {"/employee/{employeeId}/delete"})
    public String showDeleteemployeeById(
            Model model, @PathVariable Long employeeId) {
        Employee employee = null;
        try {
            employee = employeeService.findById(employeeId);
        } catch (Exception ex) {
            model.addAttribute("errorMessage", "Emp not found");
        }
        model.addAttribute("allowDelete", true);
        model.addAttribute("employee", employee);
        return "employee";
    }

    @PostMapping(value = {"/employee/{employeeId}/delete"})
    public String deleteemployeeById(
            Model model, @PathVariable Long employeeId) {
        try {
            employeeService.delete(employeeId);
            return "redirect:/v1/view/employee";
        } catch (Exception ex) {
            String errorMessage = ex.getMessage();
            model.addAttribute("errorMessage", errorMessage);
            return "employee";
        }
    }
}
