package com.example.restapi.latihan12.controller;

import com.example.restapi.latihan12.entity.EmployeeDetail;
import com.example.restapi.latihan12.service.EmployeeDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("synrgy/employee-detail")
public class EmployeeDetailController {
    @Autowired
    EmployeeDetailService service;

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<Map> getAll() {
        Map employeeDetails = service.getAll();
        return new ResponseEntity<Map>(employeeDetails, HttpStatus.OK);
    }

    @GetMapping("/all/{id}")
    @ResponseBody
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map employeeDetail = service.getById(id);
        return new ResponseEntity<Map>(employeeDetail, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody EmployeeDetail employeeDetailToUpdate) {
        Map map = new HashMap();
        Map employeeDet = service.update(employeeDetailToUpdate);

        map.put("Request = ", employeeDetailToUpdate);
        map.put("Response = ", employeeDet);
        return new ResponseEntity<Map>(employeeDet, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map employeeDet = service.delete(id);
        return new ResponseEntity<Map>(employeeDet, HttpStatus.OK);
    }

    @PostMapping("/save/{idemployee}")
    public ResponseEntity<Map> save(@Valid @RequestBody EmployeeDetail newEmployeeDetail, @PathVariable("idemployee") Long idemployee) {
        Map map = new HashMap();
        Map rek = service.insert(newEmployeeDetail, idemployee);

        map.put("Request = ", newEmployeeDetail);
        map.put("Response = ", rek);
        return new ResponseEntity<Map>(rek, HttpStatus.OK);
    }

}
