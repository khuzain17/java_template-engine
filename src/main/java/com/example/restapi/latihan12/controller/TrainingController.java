package com.example.restapi.latihan12.controller;

import com.example.restapi.latihan12.entity.Training;
import com.example.restapi.latihan12.repository.TrainingRepo;
import com.example.restapi.latihan12.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("synrgy/training")
public class TrainingController {

    @Autowired
    TrainingService service;

    @Autowired
    public TrainingRepo trainingRepo;

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<Map> getAll() {
        Map trainings = service.getAll();
        return new ResponseEntity<Map>(trainings, HttpStatus.OK);
    }

    //sorting by @requestParam
    @GetMapping("/all-sorting")
    @ResponseBody
    public ResponseEntity<Map> getAllSorting(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String nameShort,
            @RequestParam() String typeShort
    ) {
        Map map = new HashMap();
        String getshorting = nameShort.equals("")?"id": nameShort;
        Pageable show_data;
        if(typeShort.equals("desc")) {
            show_data = PageRequest.of(page, size, Sort.by(getshorting).descending());
        } else  {
            show_data = PageRequest.of(page, size, Sort.by(getshorting).ascending());
        }
        map.put("data", trainingRepo.findAll(show_data));
        return new ResponseEntity<Map>(map, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/all/{id}")
    @ResponseBody
    public ResponseEntity<Map> getById(@PathVariable(value = "id") Long id) {
        Map train = service.getById(id);
        return new ResponseEntity<Map>(train, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Training newTraining) {
        Map map = new HashMap();
        Map train = service.insert(newTraining);

        map.put("Request = ", newTraining);
        map.put("Response = ", train);
        return new ResponseEntity<Map>(train, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody Training trainingToUpdate) {
        Map map = new HashMap();
        Map train = service.update(trainingToUpdate);

        map.put("Request = ", trainingToUpdate);
        map.put("Response = ", train);
        return new ResponseEntity<Map>(train, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map train = service.delete(id);
        return new ResponseEntity<Map>(train, HttpStatus.OK);
    }


}
