package com.example.restapi.latihan12.repository;

import com.example.restapi.latihan12.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

//Step-step:
//1. Repository ditambahkan setelah menentukan entity yang akan dibuat di package entity
//2. Isi dari repository adalah method yang nanti akan dipanggil di service
//3. Bagian ini berisi sintaks query

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    @Query("select e from Employee e where e.id = :id")
    public Employee getById(@Param("id") Long id);

    @Query("select e from Employee e")
    public List<Employee> getAllEmployee();

    @Query("select e from Employee e where e.status = :status")
    public List<Employee> getByStatus(@Param("status") int status);
}
