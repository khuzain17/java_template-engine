package com.example.restapi.latihan12.repository;

import com.example.restapi.latihan12.entity.Rekening;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RekeningRepo extends JpaRepository<Rekening, Long> {
    @Query("select e from Rekening e where e.id = :id")
    public Rekening getById(@Param("id") Long id);

    @Query("select e from Rekening e")
    public List<Rekening> getAllRekening();

    @Transactional
    @Query(value = "UPDATE rekening \n" +
            "SET deleted_date = e2.deleted_date \n" +
            "FROM employee e2 \n" +
            "WHERE rekening.employee_id = e2.id ;", nativeQuery = true)
    public Rekening updateDeletedDate();
}
