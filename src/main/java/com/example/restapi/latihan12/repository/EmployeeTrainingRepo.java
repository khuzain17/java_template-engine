package com.example.restapi.latihan12.repository;

import com.example.restapi.latihan12.entity.EmployeeTraining;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeTrainingRepo extends PagingAndSortingRepository<EmployeeTraining, Long> {
    @Query("select e from EmployeeTraining e")
    public List<EmployeeTraining> getAllEmployeeTraining();

    @Query("SELECT k FROM EmployeeTraining k WHERE k.id = :id")
    EmployeeTraining getById(@Param("id") long id);
}
