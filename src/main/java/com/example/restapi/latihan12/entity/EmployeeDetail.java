package com.example.restapi.latihan12.entity;

import com.example.restapi.latihan12.entity.Abstract.AbstractDate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "employee_detail")
@Where(clause = "deleted_date is null")
public class EmployeeDetail extends AbstractDate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nik", length = 35)
    private String nik;

    @Column(name = "npwp", length = 35)
    private String npwp;

//    @JsonIgnore
//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "id_employee", referencedColumnName = "id")
//    private Employee employee;

    @JsonIgnore
    @OneToOne(targetEntity = Employee.class, cascade = CascadeType.ALL)
    private Employee employee;

}
